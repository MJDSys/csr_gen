extern crate rustc_serialize;
extern crate toml;

use std::env;
use std::io::prelude::*;
use std::collections::BTreeMap;
use std::fs::File;
use std::process::*;

#[derive(RustcDecodable, RustcEncodable,Debug,Default)]
struct Config {
    key: String,
    csrs: BTreeMap<String, Vec<String>>,
}

fn main() {
    let config_file = env::args().skip(1).next().unwrap();
    let mut f = File::open(config_file).unwrap();

    let mut contents = String::new();
    f.read_to_string(&mut contents).unwrap();
    let contents = contents;

    let config: Config = toml::decode_str(&contents).unwrap();

    for (csr, domains) in config.csrs.iter() {
        let mut subj_str = "/CN=".to_string();
        subj_str.push_str(&domains[0]);
        let subj_str = subj_str;

        let mut config_str = "[req]\ndistinguished_name = a\n[a]\n\n[SAN]\nsubjectAltName="
                                 .to_string();
        for domain in domains {
            config_str.push_str("DNS:");
            config_str.push_str(&domain);
            config_str.push_str(",");
        }
        let new_len = config_str.len() - 1;
        config_str.truncate(new_len);
        let config_str = config_str;

        let mut child = Command::new("openssl")
                            .arg("req")
                            .arg("-new")
                            .arg("-key")
                            .arg(&config.key)
                            .arg("-sha512")
                            .arg("-subj")
                            .arg(&subj_str)
                            .arg("-reqexts")
                            .arg("SAN")
                            .arg("-config")
                            .arg("/dev/stdin")
                            .arg("-out")
                            .arg(&csr)
                            .stdin(Stdio::piped())
                            .spawn()
                            .unwrap();

        {
            child.stdin.as_mut().unwrap().write_all(config_str.as_bytes()).unwrap();
        }

        let result = child.wait().unwrap();

        println!("Details! {:?} {:?} {:?} {}",
                 &csr,
                 subj_str,
                 config_str,
                 result);
    }
}
